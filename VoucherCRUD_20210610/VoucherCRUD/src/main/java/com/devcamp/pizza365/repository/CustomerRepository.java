package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.model.CCountry;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT COUNT(country) as c_cou, country FROM customers GROUP BY country", nativeQuery = true)
    List<Object[]> findCustomerCountByCountry();

}
